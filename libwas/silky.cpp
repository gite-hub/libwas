#include "silky.h"
#include "efolder.h"
#include "cWas.h"
#include <fstream>

void getSilky(const std::string& pathLua, std::set<uint>& us)
{
	const auto& luas = cc::efolder(pathLua, false, 0);
	std::ifstream ifile;
	std::string line;
	int i, i1, n, size;
	uint u;
	bool is0x;
	for (const auto& lua : luas)
	{
// 		if (lua.find("ս��ģ�Ϳ�") != std::string::npos)
// 		{
// 			continue;
// 		}
		ifile.open(lua, std::ios::binary);
		if (!ifile.is_open())
		{
			continue;
		}
		while (true)
		{
			getline(ifile, line);
			size = line.length();
			i = 0;
			i1 = -1;
			while (i < size)
			{
				if (i <= size - 10 && line[i] == '0' && (line[i + 1] == 'x' || line[i + 1] == 'X'))
				{
					const auto& c = line[i + 5];
					if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
					{
						//	const auto& c2 = line[i + 9];
						//	if ((c2 >= '0' && c2 <= '9') || (c2 >= 'a' && c2 <= 'f') || (c2 >= 'A' && c2 <= 'F'))
						{
							us.insert(ccc_s2u(line.substr(i, 10)));
							i += 10;
							continue;
						}
					}
				}
				else if (line[i] >= '0' && line[i] <= '9')
				{
					if (i1 == -1)
					{
						i1 = i;
						n = 1;
					}
					else
					{
						++n;
					}
				}
				else
				{
					if (i1 != -1)
					{
						u = std::atoll(line.substr(i1, n).c_str());
						//	if(u >= 0x10000000)
						us.insert(u);
						i1 = -1;
					}
				}
				if (++i == size)
				{
					if (i1 != -1)
					{
						u = std::atoll(line.substr(i1, n).c_str());
						//	if (u >= 0x10000000)
						us.insert(u);
						i1 = -1;
					}
					break;
				}
			}
			if (ifile.eof())
			{
				break;
			}
		}
		ifile.close();
	}
}

void writeSilky(std::set<uint>& us, const std::string& pathZlpud, const std::string& zlpui, const std::string& pathSilky, const std::string& fileout)
{
	std::array<cWas, 5> zls;
	std::vector<std::string> names = { "ui.wdf", "baby.wdf", "shape.wdf", "shape.wd2", "uijd.wdf" };
	zls[0].load(zlpui);
	for (int k = 1; k < 5; ++k)
	{
		zls[k].load(pathZlpud + names[k]);
	}
	const auto& ces = cc::efolder(pathSilky, true, 0);
// 	std::vector<std::string> ces = { "magic.wdf" };
// 	if (1)
// 	{
// 		cWas was;
// 		was.load(s_desktop + "�ݶ���/wdf/" + ces.front());
// 		auto idx = was.getIndexs();
// 		for (int i = was.getIndexsCount() - 1; i >= 0; --i, ++idx)
// 		{
// 			us.insert(idx->uid);
// 		}
// 	}

	int size, siz2;
	std::string path;
	std::ofstream ofile, otxt(fileout);
	for (const auto& c : ces)
	{
		//	if(c != "ZHS.FT") continue;
		cWas was;
		was.load(pathSilky + c);
		bool md = false;
		otxt << toString("[\"%s\"]={\n", c.c_str());
		printf("\n%s\n", c.c_str());
		int n = 0;
		for (const auto& u : us)
		{
			auto ptr = was.getData(u, size);
			if (ptr == nullptr)
			{
				continue;
			}
			++n;
			bool z = false;
			forv(zls, k)
			{
				auto p = zls[k].getData(u, siz2);
				if (p == nullptr)
				{
					continue;
				}
				if (size == siz2 && std::string(ptr, size) == std::string(p, siz2))
				{
					otxt << toString("    [0x%08X]={ file=\"%s\" },\n", u, names[k].c_str());
					z = true;
					delete[] p;
					break;
				}
				delete[] p;
			}
			if (!z)
			{
				bool found = false;
				forv(zls, k)
				{
					auto& zl = zls[k];
					auto idx = zl.getIndexs();
					for (int i = zl.getIndexsCount() - 1; i >= 0; --i, ++idx)
					{
						if ((int)idx->size != size)
						{
							continue;
						}
						auto p = zl.getData(idx);
						if (std::string(ptr, size) == std::string(p, size))
						{
							otxt << toString("    [0x%08X]={ file=\"%s\", id = 0x%08X },\n", u, names[k].c_str(), idx->uid);
							found = true;
							delete[] p;
							break;
						}
						delete[] p;
					}
					if (found)
					{
						z = true;
						break;
					}
				}
			}
			if (!z)
			{
				path = pathSilky + c + ".unpack/";
				if (!md)
				{
					md = true;
					ccc_md(path);
				}
				ofile.open(path + ccc_u2was(u), std::ios::binary);
				ofile.write(ptr, size);
				ofile.close();
			}
			delete[] ptr;
		}
		otxt << "},\n";
		printf("%d", n);
	}
	otxt.close();
}


void writeSilky(const std::string& pathSilky, const std::string& name, const std::string& pathZlp, const std::string& nam2, const std::string& fileout)
{
	cWas was, zlp;
//	std::string name("music.wdf"), nam2("music.wdf");
	std::string path = pathSilky + name;
	was.load(path);
	zlp.load(pathZlp + nam2);
	path += s_unpack;
	std::ofstream ofile, otxt(fileout);
	otxt << toString("[\"%s\"]={\n", name.c_str());
	bool md = false;
	auto ix = was.getIndexs();
	for (int i = was.getIndexsCount() - 1; i >= 0; --i, ++ix)
	{
		bool z = false;
		auto ptr = was.getData(ix);
		auto idx = zlp.search2(ix->uid);
		if (idx != nullptr && idx->size == ix->size)
		{
			auto p = zlp.getData(idx);
			if (std::string(ptr, ix->size) == std::string(p, idx->size))
			{
				otxt << toString("    [0x%08X]={ file=\"%s\" },\n", ix->uid, nam2.c_str());
				z = true;
			}
			delete[] p;
		}
		if (!z)
		{
			idx = zlp.getIndexs();
			for (int ii = zlp.getIndexsCount() - 1; ii >= 0; --ii, ++idx)
			{
				if (idx->size != ix->size)
				{
					continue;
				}
				auto p = zlp.getData(idx);
				if (std::string(ptr, ix->size) == std::string(p, idx->size))
				{
					otxt << toString("    [0x%08X]={ file=\"%s\", id = 0x%08X },\n", ix->uid, nam2.c_str(), idx->uid);
					z = true;
					delete[] p;
					break;
				}
				delete[] p;
			}
		}
		if (!z)
		{
			if (!md)
			{
				md = true;
				ccc_md(path);
			}
			ofile.open(path + ccc_u2was(ix->uid), std::ios::binary);
			ofile.write(ptr, ix->size);
			ofile.close();
		}

		delete[] ptr;
	}
	otxt << "}\n";
	otxt.close();
}


std::vector<std::string> cutSilkyMap(const std::string& pathScene, const std::string& pathZlp)
{
	auto cs = cc::efolder(pathScene, true, 0);
	auto ms = cc::efolder(pathZlp + "scene.ori/", true, 0);
	for (const auto& m : ms)
	{
		forr(cs, k)
		{
			if (cs[k] == m)
			{
				cs.erase(cs.begin() + k);
				break;
			}
		}
	}
	ms = cc::efolder(pathZlp + "scene.jd/", true, 0);
	for (const auto& m : ms)
	{
		forr(cs, k)
		{
			if (cs[k] == m)
			{
				cs.erase(cs.begin() + k);
				break;
			}
		}
	}
	ms = cc::efolder(pathZlp + "scene.it/", true, 0);
	for (const auto& m : ms)
	{
		forr(cs, k)
		{
			if (cs[k] == m)
			{
				cs.erase(cs.begin() + k);
				break;
			}
		}
	}
	return std::move(cs);
}
