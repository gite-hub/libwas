#include "readers.h"
#include "efolder.h"
#include "cWas.h"


const bool sSpCompare::operator()(const sSpInfo& s2, const sSpInfo& s) const{
	if (s2.width != s.width)return s2.width < s.width;
	if (s2.height != s.height)return s2.height < s.height;
	if (s2.kx != s.kx)return s2.kx < s.kx;
	if (s2.ky != s.ky)return s2.ky < s.ky;
	if (s2.directionsCount != s.directionsCount)return s2.directionsCount < s.directionsCount;
	if (s2.framesCountPerDirection != s.framesCountPerDirection)return s2.framesCountPerDirection < s.framesCountPerDirection;
	if (s2.framesCount != s.framesCount) return s2.framesCount < s.framesCount;
	for (int k = 0; k < s.framesCount; ++k)
	{
		const auto& f2 = s2.frames[k];
		const auto& f = s.frames[k];
		if (f2.w != f.w)return f2.w < f.w;
		if (f2.h != f.h)return f2.h < f.h;
		if (f2.x != f.x)return f2.x < f.x;
		if (f2.y != f.y)return f2.y < f.y;
	}
	return false;
}




const bool pSpCompare::operator()(const sSpInfo* s2, const sSpInfo* s) const {
	if (s2->width != s->width)return s2->width < s->width;
	if (s2->height != s->height)return s2->height < s->height;
	if (s2->kx != s->kx)return s2->kx < s->kx;
	if (s2->ky != s->ky)return s2->ky < s->ky;
	if (s2->directionsCount != s->directionsCount)return s2->directionsCount < s->directionsCount;
	if (s2->framesCountPerDirection != s->framesCountPerDirection)return s2->framesCountPerDirection < s->framesCountPerDirection;
	if (s2->framesCount != s->framesCount) return s2->framesCount < s->framesCount;
	for (int k = 0; k < s->framesCount; ++k) {
		const auto& f2 = s2->frames[k];
		const auto& f = s->frames[k];
		if (f2.w != f.w)return f2.w < f.w;
		if (f2.h != f.h)return f2.h < f.h;
		if (f2.x != f.x)return f2.x < f.x;
		if (f2.y != f.y)return f2.y < f.y;
	}
	return false;
}




//////////////////////////////////////////////////////////////////////////
cReaders::cReaders()
{
	_ptr = (char*)malloc(_allocSize = 0x04FF);
}

cReaders::~cReaders()
{
	for (auto& wdf : _wdfs) delete wdf; free(_ptr);
}

bool cReaders::load(const std::string &filename)
{
	const auto& istr = filename.find_last_of("/");
	std::string path, name;
	if (istr == std::string::npos)
	{
		name = filename;
	}
	else
	{
		path = filename.substr(0, istr + 1);
		name = filename.substr(istr + 1);
	}

	sWdf* wdf = new sWdf();
	if (!wdf->load(path + name))
	{
		delete wdf;
		return false;
	}
	wdf->easyName = name;
	wdf->fullName = path + name;
	_wdfs.push_back(wdf);
	return true;
}

bool cReaders::loads(const std::string &path, int deep /* = 0 */)
{
	auto names = cc::efolder(path, false, deep);
	forv(names, k)
	{
		load(names.at(k));
	}
	if (!names.empty())
	{
		_path = path;
	}
	return true;
}

sPointer cReaders::getPointer(ulong uid)
{
	sPointer pointer;
	forv(_wdfs, k)
	{
		sWdf* wdf = _wdfs.at(k);
		auto ptr = wdf->getData(uid, pointer.size);
		if (ptr == nullptr)
		{
			continue;
		}
		if (_allocSize < pointer.size)
		{
			_ptr = (char*)realloc(_ptr, _allocSize = pointer.size);
		}
		memcpy(_ptr, ptr, pointer.size);
		pointer.ptr = _ptr;
		break;
	}
	return pointer;
}
