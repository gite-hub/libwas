#pragma once
#include "_inc.h"

void getSilky(const std::string& pathLua, std::set<uint>& uids);


void writeSilky(std::set<uint>& uids, const std::string& pathZlpud, const std::string& zlpui, const std::string& pathSilky, const std::string& fileout);


void writeSilky(const std::string& pathSilky, const std::string& name, const std::string& pathZlp, const std::string& nameZlp, const std::string& fileout);


std::vector<std::string> cutSilkyMap(const std::string& pathScene, const std::string& pathZlp);