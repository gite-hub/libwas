#pragma once
#include "_inc.h"
#include "efolder.h"
#include "readers.h"

void wasUnpack(const std::string& path, bool isNeedPrintf = false);


std::map<ulong, std::string> wasPack(const std::string& path);


// 先判断文件夹是否合格
void wasPack(std::map<ulong, std::string>& uMap, const std::string& path, const std::string& packname_include_path, bool isNeedPrintf, bool isNeedMd);



// 自动创建目录
void wasPack(const std::string& path, const std::string& packname_include_path, bool isNeedMd);




void spPacks(const std::string& pathCopyFrom, const std::string& pathUnpack, const std::string& pathRepack);