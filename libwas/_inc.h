#pragma once

#include "_cpp.h"
#include "_string.h"


#define ccc_u2s(_uid_)toString("%08X", (_uid_))
#define ccc_s2u(_str_)std::strtoul(_str_.c_str(), nullptr, 16)
#define ccc_u2was(_uid_)toString("%08X.was", (_uid_))

#define ccc_md(_path_)system(("md " + cc::replace((_path_), "/", "\\")).c_str())
#define ccc_move(_name_, _path_move_)system(("move " + cc::replace(((_name_) + " " + (_path_move_)), "/", "\\")).c_str())
#define ccc_copy(_name_, _name_copy_)system(("copy " + cc::replace(((_name_) + " " + (_name_copy_)), "/", "\\")).c_str())

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

static const int s_countInv = 100;
static const std::string s_unpack = ".unpack/";

struct sPointer
{
	char *ptr = nullptr;
	int size = 0;

	operator bool()const
	{
		return ptr != nullptr && size > 0;
	}
};



inline void writePointer(const char* ptr, int size, const std::string& filename)
{
	FILE* file = fopen(filename.c_str(), "wb");
	fwrite(ptr, 1, size, file);
	fclose(file);
}


inline void writePointer(const sPointer& pter, const std::string& filename)
{
	writePointer(pter.ptr, pter.size, filename);
}


inline sPointer mallocPointer()
{
	sPointer pointer;
	pointer.ptr = (char*)malloc(pointer.size = 0xfff);
	return pointer;
}

inline void resizePointer(sPointer& pointer, int size)
{
	if (pointer.size < size)
	{
		pointer.ptr = (char*)realloc(pointer.ptr, pointer.size = size);
	}
}


inline void sortUids(std::vector<ulong>& us)
{
	std::sort(us.begin(), us.end(), [](const ulong& a, const ulong& b) {return a < b; });
}

inline std::vector<ulong>  getIntersection(const std::vector<ulong>& uSorts, const std::vector<ulong>& uSort2s)
{
	std::vector<ulong> intersections(std::min(uSorts.size(), uSort2s.size()));
	const auto& it = set_intersection(uSort2s.begin(), uSort2s.end(), uSorts.begin(), uSorts.end(), intersections.begin());
	intersections.resize(it - intersections.begin());
	return intersections;
}


inline std::vector<ulong>  getDifference(const std::vector<ulong>& uSorts, const std::vector<ulong>& uSort2s)
{
	std::vector<ulong> intersections(std::max(uSorts.size(), uSort2s.size()));
	const auto& it = set_difference(uSort2s.begin(), uSort2s.end(), uSorts.begin(), uSorts.end(), intersections.begin());
	intersections.resize(it - intersections.begin());
	return intersections;
}

inline bool isSp(const char* ptr) { return (*(ushort*)ptr) == 0x5053; }
