#pragma once
#include "readers.h"


bool is_map_ui_scene_sound_music_stock_chat_mh_wz(const std::string& wdfname);

using sRepat = std::map<ulong, std::vector<std::string>>;
// 169 smap与common/smap互斥
// 169 smap和其他wdf有重复
// 169 common/smap只有猫屋/秘境/迷宫
// 169 scene common/scene 闭环重复（都是文本和未知，无SP）
// 169 common/ui与其他wdf有重复
// 先忽略以上（sound music stock chat 也没重复）
// 206个（不判断SP） 再忽略 same string 128个 （81个 mhimage.wdf wzimage.wdf 这两wdf都是非SP）
// 169 mhimage.wdf wzimage.wdf 也忽略
// 测试过双方要么都是SP要么都不是SP
// 30个（judgeSp）
sRepat getRepeatsFrom169(const std::string& path169, bool judgeSp);




using sSps = std::map<const sSpInfo*, ulong, pSpCompare>;
// 缓存比较 留着给乱舞唯美用
void giveSpsFromWdf(cReaders::sWdf* wdf, sSps& sps);





using sPair = std::pair<std::string, ulong>;
using sSpair = std::pair<std::string, sSps>;
// 除sReapet
bool isSameFrom172(ulong uid, const std::string& str, const std::vector<cReaders::sWdf*>& wdfs);
bool isSameFrom172(ulong uid, const sSpInfo& sp, const std::vector<sSpair>& spairs);





// 全局查找相同文件
sPair findMapFrom172(ulong uid, const std::string& str, const std::string& path172, const std::vector<cReaders::sWdf*>& wdfs);
// 
sPair findMapFrom172(ulong uid, const sSpInfo& sp, const std::vector<sSpair>& spairs);

// octopus stock talk 全覆盖
// smap.wd1 全覆盖
// shape1.wdf(m5) media.5 shape.wd1(m5color) shape.迭代 shape.经典 几乎没有映射
// media.zq 272/407 但是只差了2M估计都是头像
// color 281/1128 文件跨度太大算了
std::vector<std::array<sPair, 2>> findMapFrom172(const std::vector<cReaders::sWdf*>& dlcs, const std::string& path172, const std::vector<cReaders::sWdf*>& wdfs, const sRepat& rmap);