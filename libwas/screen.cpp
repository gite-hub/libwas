#include "screen.h"
#include "readers.h"
#include "efolder.h"
#include "pack.h"


void getAggregation(const std::vector<ulong>& uids_common, std::string path1, std::string path2, const std::string& pathout)
{
	cReaders r1, r2;
	r1.loads(path1);
	r2.loads(path2);

	bool isDF = false;
	bool isD = false;
	bool isNO = false;
	bool isDiff = false;
	bool isNoSp = false;

	// 没有/就返回字符串本身
	// 有/就返回至少两个(左右)
	std::vector<std::string> strs;
	cc::split(path1, "/", strs);
	std::string str1 = strs[strs.size() - 2];
	if (!str1.empty())
	{
		path1 = str1 + "/";
	}

	cc::split(path2, "/", strs);
	str1 = strs[strs.size() - 2];
	if (!str1.empty())
	{
		path2 = str1 + "/";
	}

	const char* strDF = "同帧/";
	const char* strD = "同向/";
	const char* strNO = "同号/";
	const char* strDiff = "差集/";

	auto rep = [](const std::string& s)
	{
		return cc::replace(s, "/", ".");
	};

	sPointer p5, p;
	sSpInfo sp5, sp;
	std::string str, str2;
	for (const auto& u : uids_common)
	{
		printf(".");
		p = r2.getPointer(u);
		if (p.ptr == nullptr || p.size == 0)
		{
			str1 = pathout + strDiff;
			if (!isDiff)
			{
				ccc_md(str1);
				isDiff = true;
			}
			p5 = r1.getPointer(u);
			writePointer(p5, str1 + ccc_u2was(u));
			continue;
		}
		sp.load(p.ptr, false);
		p5 = r1.getPointer(u);
		if (!isSp(p5.ptr))
		{
			if (!isNoSp)
			{
				ccc_md(pathout + "notsp/");
				isNoSp = true;
			}
			writePointer(p5, pathout + "notsp/" + ccc_u2was(u));
			continue;
		}
		sp5.load(p5.ptr, false);
		// 为什么不一样?
		// 因为每个pter不是单独内存
		if (sp.directionsCount == sp5.directionsCount)
		{
			if (sp.framesCountPerDirection == sp.framesCountPerDirection)
			{
				// if ((sp.dCount > 1 && (compare(sp, sp5) || compare(sp5, sp))) || (sp.dCount == 1 && string(p.ptr, p.size) != string(p5.ptr, p5.size)))
				if (std::string(p.ptr, p.size) != std::string(p5.ptr, p5.size))
				{
					str1 = pathout + strDF + path1;
					str2 = pathout + strDF + path2;
					if (!isDF)
					{
						ccc_md(str1);
						ccc_md(str2);
						isDF = true;
					}
					writePointer(p5, str1 + ccc_u2was(u));
					writePointer(p, str2 + ccc_u2was(u));
				}
			}
			else
			{
				str1 = pathout + strD + path1;
				str2 = pathout + strD + path2;
				if (!isD)
				{
					ccc_md(str1);
					ccc_md(str2);
					isD = true;
				}
				writePointer(p5, str1 + ccc_u2was(u));
				writePointer(p, str2 + ccc_u2was(u));
			}
		}
		else
		{
			str1 = pathout + strNO + path1;
			str2 = pathout + strNO + path2;
			if (!isNO)
			{
				ccc_md(str1);
				ccc_md(str2);
				isNO = true;
			}
			writePointer(p5, str1 + ccc_u2was(u));
			writePointer(p, str2 + ccc_u2was(u));
		}
	}

	if (isDF)
	{
		str1 = pathout + strDF + path1;
		str2 = pathout + strDF + path2;
		wasPack(str1, pathout + rep(strDF + path1), false);
		wasPack(str2, pathout + rep(strDF + path2), false);
	}
	if (isD)
	{
		str1 = pathout + strD + path1;
		str2 = pathout + strD + path2;
		wasPack(str1, pathout + rep(strD + path1), false);
		wasPack(str2, pathout + rep(strD + path2), false);

	}
	if (isNO)
	{
		str1 = pathout + strNO + path1;
		str2 = pathout + strNO + path2;
		wasPack(str1, pathout + rep(strNO + path1), false);
		wasPack(str2, pathout + rep(strNO + path2), false);
	}
	if (isDiff)
	{
		str1 = pathout + strDiff;
		wasPack(str1, pathout + rep(std::string(strDiff) + "wdf"), false);
	}
}
