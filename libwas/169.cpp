#include "169.h"
#include <fstream>


bool is_map_ui_scene_sound_music_stock_chat_mh_wz(const std::string& wdfname){
	if (wdfname.find("smap") != std::string::npos)  return true;
	if (wdfname.find("ui") != std::string::npos)    return true;
	if (wdfname.find("scene") != std::string::npos) return true;
	if (wdfname.find("sound") != std::string::npos) return true;
	if (wdfname.find("music") != std::string::npos) return true;
	if (wdfname.find("stock") != std::string::npos) return true;
	if (wdfname.find("chat") != std::string::npos)  return true;
	if (wdfname.find("mhimage") != std::string::npos) return true;
	if (wdfname.find("wzimage") != std::string::npos) return true;
	return false;
}


sRepat getRepeatsFrom169(const std::string& path169, bool judgeSp){
	cReaders r;
	r.loads(path169, 1);
	sRepat rmapori;
	int i;
	std::string str;
	for (const auto& wdf : r.getWdfs()) {
		if (wdf->easyName.find(".wd") == std::string::npos) continue;
		if (is_map_ui_scene_sound_music_stock_chat_mh_wz(wdf->easyName)) continue;
		str = wdf->fullName.substr(path169.size(), wdf->fullName.size());
		i = 0;
		for (auto idx = wdf->getIndexs(); i < wdf->getIndexsCount(); ++i, ++idx) {
			const auto& it = rmapori.find(idx->uid);
			if (it == rmapori.end()) {
				rmapori.emplace(idx->uid, std::vector<std::string>{ str });
			} else {
				it->second.emplace_back(str);
			}
		}
	}
	decltype(rmapori) rmap;
	for (const auto& it : rmapori) {
		if (it.second.size() < 2) continue;
		rmap.emplace(it);
	}
	// 二次去重
	rmapori = rmap;
	rmap.clear();
	int size;
	for (const auto& it : rmapori) {
		cWas w1, w2;
		w1.load(path169 + it.second[0]);
		w2.load(path169 + it.second[1]);
		auto p1 = w1.getData(it.first, i);
		auto p2 = w2.getData(it.first, size);
		if (judgeSp) {
			// 双方要么都是SP要么都不是SP
			if (!isSp(p1) || !isSp(p2)) {
				delete[] p1;
				delete[] p2;
				continue;
			}
		}
		if (i != size || std::string(p1, i) != std::string(p2, size)) {
			rmap.emplace(it);
			delete[] p1;
			delete[] p2;
			continue;
		}
		// 只有1个3
		if (it.second.size() == 3) {
			cWas w3;
			w3.load(path169 + it.second[2]);
			delete[] p2;
			p2 = w3.getData(it.first, size);
			if (i != size || std::string(p1, i) != std::string(p2, size)) {
				rmap.emplace(it);
				delete[] p1;
				delete[] p2;
				continue;
			}
		}
		delete[] p1;
		delete[] p2;
	}
//	return rmap;


	std::ofstream ofile("getRepeatsFrom169.txt");
	ofile << rmap.size() << "\n";
	for (const auto& it : rmap) {
		ofile << ccc_u2s(it.first) << " " << it.second.size() << " ";
		for (const auto& s : it.second) ofile << s << " ";
		ofile << "\n";
	}
	ofile.close();
	return rmap;
}


void giveSpsFromWdf(cReaders::sWdf* wdf, sSps& sps) {
	sSpInfo sp;
	auto idx = wdf->getIndexs();
	for (int i = 0; i < wdf->getIndexsCount(); ++i, ++idx) {
		auto ptr = wdf->getData(idx);
		if (!ptr) continue;
		if (*(ushort*)ptr != 0x5053) {
			delete[] ptr;
			continue;
		}
		sp.load(ptr, true);
		if (sp.framesCount == 1) continue;
		sSpInfo* psp = new sSpInfo();
		psp->load(ptr, false);
		sps[psp] = idx->uid;
		delete[] ptr;
	}
}

bool isSameFrom172(ulong uid, const std::string& str, const std::vector<cReaders::sWdf*>& wdfs) {
	int size;
	for (const auto& wdf : wdfs) {
		auto ptr = wdf->getData(uid, size);
		if (ptr) {
			if (size == str.size()) {
				if (str == std::string(ptr, size)) {
					delete[] ptr;
					return true;
				}
			}
			delete[] ptr;
		}
	}
	return false;
}

bool isSameFrom172(ulong uid, const sSpInfo& sp, const std::vector<sSpair>& spairs) {
	for (const auto& spair : spairs) {
		for (const auto& spa : spair.second) {
			if (uid == spa.second) {
				return !(sp != *spa.first);
			}
		}
	}
	return false;
}


sPair findMapFrom172(ulong uid, const std::string& str, const std::string& path172, const std::vector<cReaders::sWdf*>& wdfs) {
	sPair pair;
	for (const auto& wdf : wdfs) {
		auto idx = wdf->getIndexs();
		for (int i = 0; i < wdf->getIndexsCount(); ++i, ++idx) {
			// 之前写到下面，拖慢了速度
			if (str.size() != idx->size)  continue;
			auto ptr = wdf->getData(idx);
			if (!ptr) continue;
			if (str != std::string(ptr, idx->size)) {
				delete[] ptr;
				continue;
			}
			pair.first = wdf->fullName.substr(path172.size(), wdf->fullName.size());
			pair.second = idx->uid;
			delete[] ptr;
			return pair;
		}
	}
	pair.first = "nil";
	pair.second = 0;
	return pair;
}



sPair findMapFrom172(ulong uid, const sSpInfo& sp, const std::vector<sSpair>& spairs) {
	sPair pair;
	for (const auto& spair : spairs) {
		const auto& it = spair.second.find(&sp);
		if (it != spair.second.end()) {
			pair.first = spair.first;
			pair.second = it->second;
			return pair;
		}
	}
	pair.first = "nil";
	pair.second = 0;
	return pair;
}





std::vector<std::array<sPair, 2>> findMapFrom172(const std::vector<cReaders::sWdf*>& dlcs, const std::string& path172, const std::vector<cReaders::sWdf*>& wdfs, const sRepat& rmap) {
	std::array<sPair, 2> pair2;
	std::vector<decltype(pair2)> umap;
#if 0
	std::vector<sSpair> spairs;
	printf("prepare:");
	forv(wdfs, i) {
		spairs.emplace_back(std::make_pair(wdfs[i]->fullName.substr(path172.size(), wdfs[i]->fullName.size()), sSps()));
	}
	forv(wdfs, i){
		printf("%d", i);
		giveSpsFromWdf(wdfs[i], spairs[i].second);
	}
	printf("\n");
#endif
	unsigned long long filesize = 0;
	for (const auto& dlc : dlcs) {
		auto idx = dlc->getIndexs();
		for (int i = 0; i < dlc->getIndexsCount(); ++i, ++idx) {
			auto ptr = dlc->getData(idx);
			if (!ptr) continue;
			std::string str(ptr, idx->size);
			bool que = rmap.find(idx->uid) == rmap.end();
			if (que && isSameFrom172(idx->uid, str, wdfs)) {
				delete[] ptr;
				continue;
			}
#if 0
			sSpInfo sp;
			sp.load(ptr, true);
			if (sp.directionsCount >= 4 && sp.framesCountPerDirection > 2) {
				sp.load(ptr, false);
				if (que && isSameFrom172(idx->uid, sp, spairs)) {
					delete[] ptr;
					continue;
				}
				pair2[1] = findMapFrom172(idx->uid, sp, spairs);
			} else
#endif
			{
				pair2[1] = findMapFrom172(idx->uid, str, path172, wdfs);
			}
			pair2[0].first = dlc->easyName;
			pair2[0].second = idx->uid;
			if (pair2[1].second == 0) {
				filesize += idx->size;
			} 
	     //   else if (rmap.find(pair2[1].second) == rmap.end())
			{
				umap.emplace_back(pair2);
				if (!(umap.size() % 100)) {
					printf("%d ", umap.size() / 100);
				}
			}
#if 0
		//	if (pair2[1].second != 0)
			{
				// 算了才2M
				str = /*(pair2[1].second == 0) ?*/ "_output/" /*: "_link/"*/;
				std::ofstream ofile(str + ccc_u2was(idx->uid), std::ios::binary);
				ofile.write(ptr, idx->size);
				ofile.close();
			}
#endif
			delete[] ptr;
		}
	}
	printf("\n map files size: %u", filesize >> 20);
#if 1
	std::ofstream ofile("映射表.txt");
	ofile << umap.size() << "\n";
	for (const auto& it : umap) {
		ofile << it[0].first << " " << ccc_u2s(it[0].second) << " " << it[1].first << " " << ccc_u2s(it[1].second) << "\n";
	}
	ofile.close();
#endif
	return umap;
}