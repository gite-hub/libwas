#pragma once
#include "_inc.h"
#include "cWas.h"

struct sSpCompare{
	// ������ж�==��
	const bool operator()(const sSpInfo& s2, const sSpInfo& s)const;
};

struct pSpCompare {
	const bool operator()(const sSpInfo* s2, const sSpInfo* s)const;
};


class cReaders
{
public:
	cReaders();
	~cReaders();

	struct sWdf : public cWas { std::string easyName, fullName; };

	bool load(const std::string &filename);

	bool loads(const std::string &path, int deep = 0);

	sPointer getPointer(ulong uid);

	const std::string& getPath() const { return _path; }
	const std::vector<sWdf*>& getWdfs()const { return _wdfs; }
private:
	std::vector<sWdf*> _wdfs;
	std::string _path;
	ulong _allocSize;
	char *_ptr;
};